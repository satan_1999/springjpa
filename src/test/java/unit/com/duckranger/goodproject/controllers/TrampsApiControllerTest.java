package unit.com.duckranger.goodproject.controllers;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.server.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.is;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.*;

import com.duckranger.goodproject.controllers.TrampsApiController;
import com.duckranger.goodproject.domain.Tramp;
import com.duckranger.goodproject.repository.TrampRepository;
import com.duckranger.goodproject.services.TrampsService;
import com.duckranger.goodproject.utils.TestDataBuilder;

public class TrampsApiControllerTest {

    @Mock
    private TrampsService       trampsService;
    @Mock
    private Environment         env;
    @Mock
    private TrampRepository     trampRepository;

    @InjectMocks
    private TrampsApiController controller;

    private MockMvc             mockMvc;

    private static final Logger logger = LoggerFactory.getLogger(TrampsApiControllerTest.class);

    @Before
    public void setUp() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testList_ShouldReturnTrampInJson() throws Exception {
        List<Tramp> tramps = Arrays.asList(TestDataBuilder.tramp(null), TestDataBuilder.tramp(null));
        when(trampsService.findAll()).thenReturn(tramps);

        ResultActions ra = this.mockMvc.perform(get("/api/tramp/list.json"));
       
        logger.info(ra.andReturn().getResponse().getContentAsString());
        
        ra.andExpect(status().isOk())
       // .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].name", is(tramps.get(0).getName())))
        .andExpect(jsonPath("$[0].distance", is(tramps.get(0).getDistance())))
        .andExpect(jsonPath("$[0].difficulty", is(tramps.get(0).getDifficulty().toString())));
    }
    /*
     * @Test public void testListByName() { fail("Not yet implemented"); }
     * 
     * @Test public void testGet() { fail("Not yet implemented"); }
     * 
     * @Test public void testSave() { fail("Not yet implemented"); }
     * 
     * @Test public void testDelete() { fail("Not yet implemented"); }
     * 
     * @Test public void testGetStudent() { fail("Not yet implemented"); }
     * 
     * @Test public void testGetStudentList() { fail("Not yet implemented"); }
     */

}
