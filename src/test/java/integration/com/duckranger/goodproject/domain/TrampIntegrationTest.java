package integration.com.duckranger.goodproject.domain;

import java.util.Properties;
import java.util.Random;

import junit.framework.Assert;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.duckranger.goodproject.domain.Difficulty;
import com.duckranger.goodproject.domain.Tramp;
import com.duckranger.goodproject.services.TrampsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath*:META-INF/servlet-context.xml" })
public class TrampIntegrationTest {

	@Autowired
	TrampsService trampsService;
	@Autowired
	private Properties  appConfig;
	@Autowired
	private Environment env;
	@Value("${db.url}")
	String url;

	@Before
	public void setup() {
	}

	@Test
	public void CreateTest() {
		Tramp t = new Tramp();
		String name = "ddd";
		t.setName(name);
		t.setDistance(new Random().nextDouble()*100);
		int pick = new Random().nextInt(Difficulty.values().length);
		t.setDifficulty(Difficulty.values()[pick]);
		trampsService.save(t);
		long id = t.getId();
		t = null;
		Tramp tt = trampsService.findOne(id);
		Assert.assertEquals(name, tt.getName());
		tt = null;
	}

//	@Test
//	public void shouldGetAppConfig() throws Exception {
//		Assert.assertNotNull(appConfig);
//		Assert.assertEquals("com.mysql.jdbc.Driver", appConfig.getProperty("db.driver"));
//		Assert.assertEquals("jdbc:mysql://localhost:3306/oas",appConfig.getProperty("db.url"));
//		Assert.assertEquals("root",appConfig.getProperty("db.username"));
//		Assert.assertEquals("root",appConfig.getProperty("db.password"));
//	}
//	
//	@Test
//	public void shouldGetConfigFromPropertySourcesPlaceholderConfigurer () throws Exception {
//		Assert.assertEquals("jdbc:mysql://localhost:3306/oas", url);
//	}
	
	/*@Test
	public void shouldGetConfigFromEnv () throws Exception {
		Assert.assertEquals("jdbc:mysql://localhost:3306/oas", env.getProperty("db.url"));
	}*/


	public TrampsService getTrampsService() {
		return trampsService;
	}

	public void setTrampsService(TrampsService trampsService) {
		this.trampsService = trampsService;
	}

}