package integration.com.duckranger.goodproject.repository;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.duckranger.goodproject.controllers.TrampsApiController;
import com.duckranger.goodproject.domain.Difficulty;
import com.duckranger.goodproject.domain.Tramp;
import com.duckranger.goodproject.repository.TrampRepository;
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(locations = { "classpath*:META-INF/servlet-context.xml" })
/*@ContextConfiguration(classes = {PersistenceContext.class})*/
/*@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })*/
public class TrampRepositoryTest {
	private static final Logger logger = LoggerFactory.getLogger(TrampsApiController.class);
	@Autowired
    private TrampRepository repository;
	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testFindAllByName_ShouldReturnData() {
		Tramp tramp = mockTramp();
		List<Tramp> tramps = repository.findAllByName(tramp.getName());
		Assert.assertTrue(tramps.size() > 0);
		Assert.assertEquals(tramp.getName(),tramps.get(0).getName());
	}

	@Test
	public final void testFindAllByDifficulty_ShouldReturnData() {
		Tramp tramp = mockTramp();
		List<Tramp> tramps = repository.findAllByDifficulty(tramp.getDifficulty());
		Assert.assertTrue(tramps.size() > 0);
		Assert.assertEquals(tramp.getDifficulty(),tramps.get(0).getDifficulty());
	}
	
	private Tramp mockTramp(){
		Tramp t = new Tramp();
		logger.info("Random String ="+RandomStringUtils.random(8,"abcdefghijklmnopqrstuvwxyz0123456789"));
		t.setName(RandomStringUtils.random(8,"abcdefghijklmnopqrstuvwxyz"));
		t.setDistance(new Random().nextDouble()*100);
		int pick = new Random().nextInt(Difficulty.values().length);
		t.setDifficulty(Difficulty.values()[pick]);
		repository.save(t);
		return t;
	}

}
