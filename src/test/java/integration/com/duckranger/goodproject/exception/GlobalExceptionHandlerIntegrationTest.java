package integration.com.duckranger.goodproject.exception;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExceptionHandlerMethodResolver;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;

import com.duckranger.goodproject.controllers.TrampsApiController;
import com.duckranger.goodproject.exception.GlobalExceptionHandler;
import com.duckranger.goodproject.services.TrampsService;

//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ActiveProfiles("test")
//@ContextConfiguration(locations = { "classpath*:META-INF/servlet-context.xml" })
public class GlobalExceptionHandlerIntegrationTest {
//    @Autowired
//    private WebApplicationContext wac;
    private MockMvc               mockMvc;
    
    @Mock
    private TrampsService trampsService;
    
    @InjectMocks
    private TrampsApiController controller;
    
//    @Autowired
//    private GlobalExceptionHandler globalExceptionHandler;
    
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandlerIntegrationTest.class);
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
       // when(trampsService.findAll()).thenThrow(new RuntimeException("SQL"));
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
     mockMvc= MockMvcBuilders.standaloneSetup(controller).setHandlerExceptionResolvers(createExceptionResolver()).build();
   //     mockMvc= MockMvcBuilders.standaloneSetup(controller, new GlobalExceptionHandler()).build();
    }
    
    private ExceptionHandlerExceptionResolver createExceptionResolver() {
        ExceptionHandlerExceptionResolver exceptionResolver = new ExceptionHandlerExceptionResolver() {
            protected ServletInvocableHandlerMethod getExceptionHandlerMethod(HandlerMethod handlerMethod, Exception exception) {
                Method method = new ExceptionHandlerMethodResolver(GlobalExceptionHandler.class).resolveMethod(exception);
                return new ServletInvocableHandlerMethod(new GlobalExceptionHandler(), method);
            }
        };
        exceptionResolver.afterPropertiesSet();
        return exceptionResolver;
    }
    
  /*  public SimpleMappingExceptionResolver createSimpleMappingExceptionResolver() {
        SimpleMappingExceptionResolver r = new SimpleMappingExceptionResolver();

//        Properties mappings = new Properties();
//        mappings.setProperty("SQLException", "databaseError");
//        mappings.setProperty("RuntimeException", "creditCardError");
r.setMappedHandlerClasses([GlobalExceptionHandler.class]);
      //  r.setExceptionMappings(mappings);  // None by default
        r.setDefaultErrorView("error");    // No default
        r.setExceptionAttribute("ex");     // Default is "exception"
        r.setWarnLogCategory("example.MvcLogger");     // No default
        return r;
    }*/

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testHandleSQLException() throws Exception {


       /* ResultActions ra = this.mockMvc.perform(get("/api/tramp/list.json"));
        logger.info(ra.andReturn().getResponse().getContentAsString());*/
        //when(trampsService.findAllByName()).
       when(trampsService.findAll()).thenThrow(new RuntimeException("SQL"));
        mockMvc.perform(get("/api/tramp/list"))
        .andDo(print());
        //.andExpect(status().isInternalServerError())
        //.andExpect(jsonPath("$.error").value("Unexpected Exception"));
    }

    /*
     * @Test public void testHandleIOException() { fail("Not yet implemented");
     * }
     * 
     * @Test public void testHandleIllegalArgumentException() {
     * fail("Not yet implemented"); }
     * 
     * @Test public void testHandleInvalidParameterException() {
     * fail("Not yet implemented"); }
     * 
     * @Test public void testHandleUnsupportedOperationException() {
     * fail("Not yet implemented"); }
     * 
     * @Test public void testHandleSecurityException() {
     * fail("Not yet implemented"); }
     * 
     * @Test public void testHandleRunTimException() {
     * fail("Not yet implemented"); }
     */

}
