package com.duckranger.goodproject.utils;

import java.util.Map;

import com.duckranger.goodproject.domain.Tramp;

public class TestDataBuilder {
    public static Tramp tramp(Map params){
        Tramp t = new Tramp();
        RandomValueFieldPopulator.populate(t, params);
        return t;
    }
    
    

}
