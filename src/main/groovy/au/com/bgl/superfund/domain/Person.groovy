package au.com.bgl.superfund.domain

import javax.persistence.Entity;


@Entity
public class Person {
  String name
  static constraints = { name blank:false }
}