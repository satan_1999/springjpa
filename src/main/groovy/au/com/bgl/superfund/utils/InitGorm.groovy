package au.com.bgl.superfund.utils
import grails.orm.bootstrap.*
import grails.persistence.*

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import au.com.bgl.superfund.domain.*

@Component
public class InitGorm {
  @Autowired
  def dataSource
  def init(){
    def init = new HibernateDatastoreSpringInitializer('au.com.bgl.superfund.domain')
    init.configureForDataSource(dataSource)
  }
  
  def getPersonCount(){
    return Person.
\
    
    ()
  }
}



