
import org.apache.commons.dbcp.BasicDataSource

import au.com.bgl.superfund.utils.InitGorm
beans {
  gormDataSource(BasicDataSource) {
    driverClassName = "org.h2.Driver"
    url = "jdbc:h2:mem:grailsDB"
    username = "sa"
    password = ""
  }
  initGorm(InitGorm){
    gormDataSource = ref("gormDataSource")
  }
}