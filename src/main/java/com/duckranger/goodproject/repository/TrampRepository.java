package com.duckranger.goodproject.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.duckranger.goodproject.domain.Difficulty;
import com.duckranger.goodproject.domain.Tramp;

public interface TrampRepository extends JpaRepository<Tramp, Long> {
	Page<Tramp> findAllByName(String name, Pageable pageable); 
	List<Tramp> findAllByName(String name); 
	List<Tramp> findAllByDifficulty(Difficulty difficulty); 
	Page<Tramp> findAllByDifficulty(Difficulty difficulty, Pageable pageable); 
}
