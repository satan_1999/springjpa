package com.duckranger.goodproject.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;

public class RandomValueFieldPopulator {

    private static final int DATE_WINDOW_MILLIS = 1000 * 60 * 60 * 60;
    private static Random    random = new Random();

    public static void populate(Object object, Map params) {
        ReflectionUtils.doWithFields(object.getClass(), new RandomValueFieldSetterCallback(object,params));
    }

    private static class RandomValueFieldSetterCallback implements FieldCallback {
        private Object targetObject;
        private Map    targetParams;

        public RandomValueFieldSetterCallback(Object targetObject, Map params) {
            this.targetObject = targetObject;
            this.targetParams = params;
        }

        @Override
        public void doWith(Field field) throws IllegalAccessException {
            Class<?> fieldType = field.getType();
            if (!Modifier.isFinal(field.getModifiers())) {
                if (targetParams != null && targetParams.containsKey(field.getName().toLowerCase())) {
                    ReflectionUtils.makeAccessible(field);
                    field.set(targetObject, targetParams.get(field.getName().toLowerCase()));
                } else {
                    Object value = generateRandomValue(fieldType);
                    if (value != null) {
                        ReflectionUtils.makeAccessible(field);
                        field.set(targetObject, value);
                    }
                }
            }
        }
    }

    public static Object generateRandomValue(Class<?> fieldType) {
        if (fieldType.equals(String.class)) {
            return RandomStringUtils.random(8, "abcdefghijklmnopqrstuvwxyzABCDEFG").toString();
            // return UUID.randomUUID().toString();
        } else if (Date.class.isAssignableFrom(fieldType)) {
            return new Date(System.currentTimeMillis() - random.nextInt(DATE_WINDOW_MILLIS));
        } else if (Number.class.isAssignableFrom(fieldType)) {
            return random.nextInt(Byte.MAX_VALUE) + 1;
        } else if (fieldType.equals(Integer.TYPE)) {
            return random.nextInt();
        } else if (fieldType.equals(Long.TYPE)) {
            return random.nextInt();
        } else if (Enum.class.isAssignableFrom(fieldType)) {
            Object[] enumValues = fieldType.getEnumConstants();
            return enumValues[new Random().nextInt(enumValues.length)];
        } else {
            return null;
        }
    }
}
