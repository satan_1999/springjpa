package com.duckranger.goodproject.exception;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ExceptionHandler(SQLException.class)
	public String handleSQLException(HttpServletRequest request, Exception ex) {
		logger.info("SQLException Occured:: URL=" + request.getRequestURL());
		return "database_error";
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occured")
	@ExceptionHandler(IOException.class)
	public void handleIOException(HttpServletRequest request, Exception ex) {
		logger.error(ex.getMessage(),ex);
		// returning 404 error code
	}
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "IOException occured")
	@ExceptionHandler(IllegalArgumentException.class)
	public void handleIllegalArgumentException(HttpServletRequest request, Exception ex) {
		logger.error(ex.getMessage(),ex);
		// returning 404 error code
	}
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The parameter is invalid")
	@ExceptionHandler(InvalidParameterException.class)
	public void handleInvalidParameterException(HttpServletRequest request, Exception ex) {
		logger.error(ex.getMessage(),ex);
		// returning 404 error code
	}
	
	@ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED, reason = "IOException occured")
	@ExceptionHandler(UnsupportedOperationException.class)
	public void handleUnsupportedOperationException(HttpServletRequest request, Exception ex) {
		logger.error(ex.getMessage(),ex);
		// returning 404 error code
	}
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "IOException occured")
	@ExceptionHandler(SecurityException.class)
	public void handleSecurityException(HttpServletRequest request, Exception ex) {
		logger.error(ex.getMessage(),ex);
		// returning 404 error code
	}
	
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Service Error")
	@ExceptionHandler(RuntimeException.class)
	public String handleRunTimException(HttpServletRequest request, Exception ex) {
		logger.error(ex.getMessage(),ex);
		
		return "handleRunTimException";
		// returning 404 error code
	}
}
