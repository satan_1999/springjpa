package com.duckranger.goodproject.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.bgl.superfund.domain.Person;
import au.com.bgl.superfund.utils.InitGorm;

import com.duckranger.goodproject.domain.Tramp;
import com.duckranger.goodproject.pojo.Student;
import com.duckranger.goodproject.pojo.StudentList;
import com.duckranger.goodproject.repository.TrampRepository;
import com.duckranger.goodproject.services.TrampsService;
import com.duckranger.goodproject.utils.RandomValueFieldPopulator;

@Controller
public class TrampsApiController {

  private static final Logger logger = LoggerFactory.getLogger(TrampsApiController.class);

  @Autowired
  private TrampsService trampsService;
  @Autowired
  private TrampRepository trampRepository;
  @Autowired
  private InitGorm initGorm;

  @RequestMapping(value = "/api/tramp/dummy", method = RequestMethod.GET)
  public String dummy() {
    logger.info("Calling dummy");

    initGorm.init();
    logger.info("Calling count" + initGorm.getPersonCount());
    Tramp t = new Tramp();
    RandomValueFieldPopulator.populate(t, null);
    trampsService.save(t);

    return "redirect:list";
  }

  /**
   * List all tramps
   */
  @RequestMapping(value = "/api/tramp/list", method = RequestMethod.GET)
  @ResponseBody
  public List<Tramp> list(@RequestParam Map<String, Object> allRequestParams) {
    logger.info("Calling list params : " + allRequestParams.toString());
    return trampsService.findAll();
  }

  @RequestMapping(value = "/api/tramp/listbyname/{name}", method = RequestMethod.GET)
  @ResponseBody
  public List<Tramp> listByName(@PathVariable("name") String name) {
    logger.info(String.format("Calling listByName with name : %s", name));
    return trampRepository.findAllByName(name, null).getContent();
  }

  // @RequestMapping(value = EmpRestURIConstants.GET_ALL_EMP, method =
  // RequestMethod.GET)
  // public @ResponseBody List<Employee> getAllEmployees() {
  // logger.info("Start getAllEmployees.");
  // List<Employee> emps = new ArrayList<Employee>();
  // Set<Integer> empIdKeys = empData.keySet();
  // for(Integer i : empIdKeys){
  // emps.add(empData.get(i));
  // }
  // return emps;
  // }

  @RequestMapping(value = "/api/tramp/{id}", method = RequestMethod.GET)
  @ResponseBody
  public Tramp get(@PathVariable("id") int empId) {
    logger.info("Start get. ID=" + empId);
    return trampsService.findOne(Long.valueOf(empId));
  }

  @RequestMapping(value = "/api/tramp/save", method = RequestMethod.POST)
  @ResponseBody
  public Tramp save(@RequestBody Tramp emp) {
    logger.info("Start save");
    trampsService.save(emp);
    return emp;
  }

  @RequestMapping(value = "/api/tramp/delete/{id}", method = RequestMethod.PUT)
  @ResponseBody
  public Map<String, String> delete(@PathVariable("id") int empId) {
    logger.info("Start delete");
    trampsService.delete(Long.valueOf(empId));
    Map<String, String> data = new HashMap<String, String>();
    return data;
  }

  @RequestMapping("/api/student")
  public @ResponseBody Student getStudent() {
    return new Student(23, "Meghna", "Naidu", "meghna@gmail.com", "8978767878");
  }

  @RequestMapping("/api/exception")
  public @ResponseBody void exception() throws SQLException {
    logger.info("Start exception");
    throw new SQLException("SQL ERROR");
    // return new Student(23, "Meghna", "Naidu", "meghna@gmail.com","8978767878");
  }

  @RequestMapping("/api/studentlist")
  public @ResponseBody StudentList getStudentList() {
    List<Student> studentList = new ArrayList<Student>();
    studentList.add(new Student(3, "Robert", "Parera", "robert@gmail.com", "8978767878"));
    studentList.add(new Student(93, "Andrew", "Strauss", "andrew@gmail.com", "8978767878"));
    studentList.add(new Student(239, "Eddy", "Knight", "knight@gmail.com", "7978767878"));

    return new StudentList(studentList);
  }

}