package com.duckranger.goodproject.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.duckranger.goodproject.services.TrampsService;


@Controller
public class TrampsController {

    private static final Logger logger = LoggerFactory.getLogger(TrampsController.class);

    @Autowired
    private TrampsService trampsService;
    @Autowired
    private Environment env;
//    @Autowired
//	private Properties  appConfig;
    /**
     * List all tramps
     */
    @RequestMapping(value = "/tramps", method = RequestMethod.GET)
    public String list(Model model) {
	//logger.info("Listing Walks"+ appConfig.getProperty("db.url"));
	logger.info("Listing Walks 2 "+ env.getProperty("db.url"));
	model.addAttribute("tramps", trampsService.findAll());
	return "tramps/list";
    }

}